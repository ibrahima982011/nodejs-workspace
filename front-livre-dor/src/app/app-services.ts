import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class AppService {
  url: string = 'http://localhost:8082';

  constructor(private http: HttpClient) {}

  sendComment(data): Observable<HttpResponse<any>>{
    console.log(data);
    return this.http.post<any>(this.url + '/messages', {'content': data}, {
      observe: 'response'
    });
  }
  getComments(): Observable<HttpResponse<any[]>>{
    return this.http.get<any[]>(this.url + '/messages', { observe: 'response' });
  }
}

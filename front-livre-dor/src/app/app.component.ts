import { Component, OnInit } from '@angular/core';
import { ErrorStateMatcher } from '@angular/material';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { AppService } from './app-services';

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'Frontend du Livre d\'or';
  comment = new FormControl('', [
    Validators.required
  ]);
  matcher = new MyErrorStateMatcher();
  comments: any[];

  constructor(private appService: AppService) {}

  ngOnInit() {
    this.appService.getComments()
      .subscribe(res => {
        this.comments = res.body;
        console.log(this.comments);
      });
  }

  sendComment() {
    console.log(this.comment.value);
    this.appService.sendComment(this.comment.value)
      .subscribe(message => this.comment.setValue(null));
  }
}

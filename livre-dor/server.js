let express = require('express')
let bodyParser = require('body-parser')
let session = require('express-session')

let app = express()

app.set('view engine', 'ejs')

//Les middlewares
app.use('/assets', express.static('public'))
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(session({
    secret: 'traore',
    resave: false,
    saveUninitialized: true,
    cookie: { secure: false }
  })
)
app.use(require('./middlewares/flash'))

//Les routes
app.get('/', (req, res) => {
    /* if (req.session.error) {
        res.locals.error = req.session.error
        req.session.error = undefined
    } */
    console.log(process.env.PORT)
    let Message = require('./models/message')

    Message.loadAll((result) => {
        res.locals.messages = result
        result.forEach(message => {
            console.log('Message: ' + message.content);
        });
        res.render('pages/index')
    })
    //console.log(res.locals);
    //res.render('pages/index', {messages: result});
})

app.post('/messages', (req, res) => {
    if (req.body.message === undefined || req.body.message === '') {
        //req.session.error = 'Le message est vide !'
        req.flash('error', 'Le message est vide !')
        res.redirect('/')
        //res.render('pages/index', {error: 'Le message est vide !'})
    } else {
        let Message = require('./models/message')

        Message.create(req.body.message, () => {
            req.flash('success', 'Merci pour le commentaire !')
            res.redirect('/')
        })
        
    }
})

app.get('/messages/:id', (req, res) => {
    let Message = require('./models/message')
    Message.loadOne(req.params.id, (result) => {
        res.locals.message = result
        res.render('pages/detail')
    })
})

app.listen(8081);
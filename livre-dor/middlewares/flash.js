module.exports = (req, res, next) => {

    //On stock le message dans la response si l'objet flash existe
    if (req.session.flash) {
        res.locals.flash = req.session.flash
        req.session.flash = undefined
    }

    //On crée la méthode flash servant à créer un objet de session 'flash'
    req.flash = (type, content) => {
        if (req.session.flash === undefined) {
            req.session.flash = {}
        }
        req.session.flash[type] = content
    }
    next()
}
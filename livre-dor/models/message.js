let con = require('../config/db')
let moment = require('moment')

class Message {

    constructor (row) {
        this.row = row
    }

    get id() {
        return this.row.id
    }

    get content() {
        return this.row.content
    }

    get created_at() {
        return moment(this.row.created_at)
    }

    static create(content, callback) {
        con.query('INSERT INTO message SET content = ?, created_at = ?',
            [content, new Date()], (err, result) => {
                if (err) throw err
                callback(result)
            })
    }

    static loadAll(callback) {
        con.query('SELECT * FROM message', (err, result) => {
                if (err) throw err
                callback(result.map((row) => new Message(row)))
            }
        )
    }

    static loadOne(id, callback) {
        con.query('SELECT * FROM message WHERE id = ?', [id], (err, result) => {
                if (err) throw err
                callback(new Message(result[0]))
            }
        )
    }
}

module.exports = Message
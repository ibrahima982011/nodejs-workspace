import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";

export enum StatutVehicule {
    ENSERVICE = 'EN SERVICE',
    ENPANNE = 'EN PANNE',
    ENREPARATION = 'EN REPARATION'
}

@Entity()
export class Vehicule {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    immatriculation: string;

    @Column()
    nom: string;

    @Column()
    marque: string;

    @Column()
    nbrePlace: number;

    @Column()
    image: string;

    @Column({name: 'statut_vehicule'})
    statut: StatutVehicule;
}
import {Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import {Client} from './Client';
import {Vehicule} from "./Vehicule";
import {Chauffeur} from "./Chauffeur";

export enum StatutContrat {
    ENCOUR = 'ENCOUR',
    ANNULE = 'ANNULE',
    SUSPENDU = 'SUSPENDU',
    VALIDE = 'VALIDE',
    TERMINE = 'TERMINE'
}

@Entity()
export class Contrat {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({nullable: false})
    numero: string;

    @Column()
    nom: string;

    @Column({nullable: false})
    duree: number;

    @Column({nullable: false})
    debut: Date;

    @Column({nullable: false})
    fin: Date;

    @Column({nullable: false})
    createdAt: Date;

    @Column({nullable: false, name: 'statut_contrat'})
    statut: StatutContrat;

    @ManyToOne(type => Client, {nullable: false})
    @JoinColumn({ name: "client_id" })
    client: Client;

    @ManyToOne(type => Vehicule,{nullable: false})
    @JoinColumn({ name: "vehicule_id" })
    vehicule: Vehicule;

    @ManyToOne(type => Chauffeur,{nullable: false})
    @JoinColumn({ name: "chauffeur_id"})
    chauffeur: Chauffeur;
}
import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";

export enum StatutChauffeur {
    DISPONIBLE = 'DISPONIBLE',
    INDISPONIBLE = 'INDISPONIBLE'
}

@Entity()
export class Chauffeur {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({nullable: false})
    nom: string;

    @Column({nullable: false})
    prenom: string;

    @Column({nullable: false, name: 'numero_permis'})
    numeroPermis: string;

    @Column({nullable: false})
    telephone: string;

    @Column()
    adresse: string;

    @Column({nullable: false, name: 'statut_chauffeur'})
    statut: StatutChauffeur;
}
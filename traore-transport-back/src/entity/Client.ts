import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";

export enum TypeClient {
    INDIVIDUEL = 'INDIVIDUEL',
    ENTREPRISE = 'ENTREPRISE'
}

@Entity()
export class Client {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({nullable: false})
    nom: string;

    @Column()
    prenom: string;

    @Column({nullable: false})
    telephone: string;

    @Column()
    email: string;

    @Column()
    adresse: string;

    @Column({nullable: false, name: 'type_client'})
    type: TypeClient;
}

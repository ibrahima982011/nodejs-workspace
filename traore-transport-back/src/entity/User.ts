import {Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn} from "typeorm";
import {Role} from "./Role";

export enum StatutUser {
    ACTIVE = 'ACTIVE',
    DESACTIVE = 'DESACTIVE',
    SUSPENDU = 'SUSPENDU'
}

@Entity()
export class User {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    prenom: string;

    @Column()
    nom: string;

    @Column({nullable: false})
    email: string;

    @Column()
    telephone: string;

    @Column()
    adresse: string;

    @Column({nullable: false})
    password: string;

    @Column({nullable: false, name: 'statut_user'})
    statut: StatutUser;

    @ManyToOne(type => Role, {nullable: false})
    @JoinColumn({ name: "role_id" })
    role: Role;

}

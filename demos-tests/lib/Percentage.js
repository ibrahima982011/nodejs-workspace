module.exports = {

    evolution: (a, b) => {
        if (a === 0) {
            return Infinity;
        }
        return round(100 * (b - a) / a);
    },
    wait: (time, callback) => {
        setTimeout(() => {
            callback(19);
        }, time)
    }
}

round = (value) => {
    return Math.round(100 * value) / 100;
}
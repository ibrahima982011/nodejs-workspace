var Percentage = require('../lib/Percentage');
var assert = require('assert');

describe('Percentage', () => {

    before(() => {
        console.log('===== Debut du test global =====');
    })
    beforeEach(() => {
        console.log('===== Debut du test unitaire =====');
    })

    describe('#evolution', () => {

        it('should give an evolution', () => {
            assert.equal(Percentage.evolution(100, 200), 100);
            assert.equal(Percentage.evolution(100, 550), 450);
            assert.equal(Percentage.evolution(100, 50), -50);
        })

        it.skip('should handle 0 evolution', () => {
            assert.equal(Percentage.evolution(0, 100), Infinity);
        })

        it('should round values', () => {
            assert.equal(Percentage.evolution(30, 100), 233.33);
        })
    })

    describe('#wait', () => {

        it('should exist', () => {
            assert.notEqual(Percentage.wait, undefined)
        })

        it('should wait', (done) => {
            Percentage.wait(100, (test) => {
                assert.equal(test, 19);
                done();
            })
        })
    })
})
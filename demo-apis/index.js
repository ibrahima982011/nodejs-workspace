// Les imports
const port = 8082;
let express = require('express');
let bodyParser = require('body-parser');
let apiRouter = require('./apiRouter').router;

// Instanciation
let server = express();

//Les Middlewares
server.use(bodyParser.urlencoded({ extended: true }));
server.use(bodyParser.json());

//Les routes
server.get('/', (req, res) => {
    //res.setHeader('Content-Type', 'application/json');
    res.status(200).send({'message': 'Hello world'});
});

server.use('/api', apiRouter);

// launch the server
server.listen(port, () => {
    console.log('Serveur lancé sur le port ' + port);
    const all_routes = require('express-list-endpoints')
    console.log(all_routes(server))
    console.log('====== // Waiting Logs // ======')
})
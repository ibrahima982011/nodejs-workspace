let jwt = require('jsonwebtoken');

const JWT_SIGN_SECRET = '0sdfdf454d5f4d8df455fr8trr4g8rgr457d845f4f7g8f4gf7gr';

module.exports = {
    generationToken: (userData) => {
        return jwt.sign({
            userId: userData.id,
            isAdmin: userData.isAdmin
        },
        JWT_SIGN_SECRET,
        {
            expiresIn: '1h'
        })
    },
    parseAuthorization: (authorization) => {
        return (authorization != null) ? authorization.replace('Bearer ', '') : null;
    },
    getUserId: (authorization) => {
        let userId = -1;
        let token = module.exports.parseAuthorization(authorization);
        if (token != null) {
            try {
                let jwtToken = jwt.verify(token, JWT_SIGN_SECRET);
                if (jwtToken != null) userId = jwtToken.userId;
            } catch (error) {
                console.log(error);
            }
        }
        return userId;
    }
}
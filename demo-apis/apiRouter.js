// Imports
let express = require('express');
let userCtrl = require('./routes/userCtrl');
let messageCtrl = require('./routes/messageCtrl');
let likeCtrl = require('./routes/likeCtrl');

//Router
exports.router = (() => {
    const apiRouter = express.Router();

    //Users Routes
    apiRouter.route('/users/register/').post(userCtrl.register);
    apiRouter.route('/users/login/').post(userCtrl.login);
    apiRouter.route('/users/profile/').get(userCtrl.getUserProfil);
    apiRouter.route('/users/profile/').put(userCtrl.updateUserProfile);

    //Messages Routes
    apiRouter.route('/messages/').post(messageCtrl.createMessage);
    apiRouter.route('/messages/').get(messageCtrl.getAllMessages);

    //Likes Routes
    apiRouter.route('/messages/:messageId/vote/like').post(likeCtrl.likePost);

    return apiRouter;
})();
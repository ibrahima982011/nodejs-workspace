// Imports
let models = require('../models');
let jwtUtils = require('../utils/jwt.util');
let asyncLib = require('async');

// Constantes
const TITLE_LIMIT = 20;
const CONTENT_LIMIT = 250;

// Routes
module.exports = {
    createMessage: (req, res) => {
        // Getting auth header
        let headerAuth = req.headers['authorization'];
        let userId = jwtUtils.getUserId(headerAuth);
        if (userId < 0) {
            return res.status(400).json({ 'error': 'Token incorrect !' });
        }

        // Params
        let title = req.body.title;
        let content = req.body.content;

        if (title == null || content == null) {
            return res.status(400).json({'error': 'Paramètre manquants !'});
        }
        if (title.length >= TITLE_LIMIT || content.length > CONTENT_LIMIT) {
            return res.status(400).json({'error': 'Paramètre invalides !'});
        }

        asyncLib.waterfall([
            (fait) => {
                models.User.findOne({
                    where: { id: userId }
                })
                .then((userFound) => fait(null, userFound))
                .catch((err) => {
                    return res.status(500).json({'error': '[CREATE_MESSAGE] Enable to verify user with this token !'});
                });
            },
            (userFound, fait) => {
                if (userFound) {
                    models.Message.create({
                        title   : title,
                        content : content,
                        likes   : 0,
                        UserId  : userFound.id
                    }).then((newMessage) => {
                        fait(newMessage);
                    });
                } else {
                    return res.status(400).json({'error': '[CREATE_MESSAGE] Enable to find this user !'});
                }
            }
        ], (newMessage) => {
            if (newMessage) {
                return res.status(201).json(newMessage);
            } else {
                return res.status(500).json({'error': '[CREATE_MESSAGE] Enable to add user !'});
            }
        });

    },
    getAllMessages: (req, res) => {
        // Getting auth header
        let headerAuth = req.headers['authorization'];
        let userId = jwtUtils.getUserId(headerAuth);
        if (userId < 0) {
            return res.status(400).json({ 'error': 'Token incorrect !' });
        }

        //Params
        let fields = req.body.fields;
        let limit = parseInt(req.body.limit);
        let offset = parseInt(req.body.offset);
        let order = req.body.order;

        models.Message.findAll({
            order: [ (order != null) ? order.split(':') : ['title', 'ASC']],
            attributes: (fields !== '*' && fields != null) ? fields.split(',') : null,
            limit: (!isNaN(limit)) ? limit : null,
            offset: (!isNaN(offset)) ? offset : null, 
            include: [{
                model: models.User,
                attributes: ['username', 'email']
            }]
        })
        .then((users) => {
            if (users)
                return res.status(200).json(users);
            else 
                return res.status(404).json({'error': err.message});
        })
        .catch((err) => {
            return res.status(500).json({'error': err.message});
        });
        
    }
}
// Imports
let models = require('../models');
let jwtUtils = require('../utils/jwt.util');
let asyncLib = require('async');

// Constantes
const LIKED = 1;

// Routes
module.exports = {
    likePost: (req, res) => {
        // Getting auth header
        let headerAuth = req.headers['authorization'];
        let userId = jwtUtils.getUserId(headerAuth);
        if (userId < 0) {
            return res.status(400).json({ 'error': '[LIKE] Token incorrect !' });
        }

        //Params
        let messageId = parseInt(req.params.messageId);
        if (messageId <= 0) {
            return res.status(400).json({ 'error': '[LIKE] Invalid parameters !' });
        }

        asyncLib.waterfall([
            (fait) => {
                models.Message.findOne({
                    where: { id: messageId }
                })
                .then((msgFound) => {
                    fait(null, msgFound);
                })
                .catch((err) => {
                    return res.status(400).json({ 'error': '[LIKE] ' + err.message });
                });
            },
            (msgFound, fait) => {
                if (msgFound) {
                    models.User.findOne({
                        where: { id: userId }
                    })
                    .then((userFound) => {
                        fait(null, msgFound, userFound);
                    }).catch((err) => {
                        return res.status(400).json({ 'error': '[LIKE] Unable to verify User' });
                    });
                } else {
                    return res.status(404).json({ 'error': '[LIKE] Post déjà liké' });
                }
            },
            (msgFound, userFound, fait) => {
                if (userFound) {
                    models.Like.findOne({
                        where: { 
                            userId: userId,
                            messageId: messageId
                        }
                    })
                    .then((userAlreadyLikedFound) => {
                        fait(null, msgFound, userFound, userAlreadyLikedFound);
                    })
                    .catch((err) => {
                        return res.status(500).json({ 'error': '[LIKE] Unable to verify if User already liked' });
                    });
                } else {
                    return res.status(409).json({ 'error': '[LIKE] User not found' });
                }
            },
            (msgFound, userFound, userAlreadyLikedFound, fait) => {
                if (!userAlreadyLikedFound) {
                    msgFound.addUser(userFound, { isLike: LIKED })
                    .then((alreadyLikedFound) => {
                        fait(null, msgFound, userFound);
                    })
                    .catch((err) => {
                        return res.status(500).json({ 'error': '[LIKE] Unable to set user action //==> ' + err.message });
                    });
                } else {
                    if (userAlreadyLikedFound.isLike !== LIKED) {
                        userAlreadyLikedFound.update({ isLike: LIKED })
                        .then(() => {
                            fait(null, msgFound, userFound);
                        })
                        .catch((err) => {
                            return res.status(500).json({ 'error': '[LIKE] Unable to update user action //==> ' + err.message });
                        });
                    } else {
                        return res.status(409).json({ 'error': '[LIKE] Message already liked' });
                    }
                }
            },
            (msgFound, userFound, fait) => {
                msgFound.update({
                    likes: msgFound.likes + 1
                })
                .then(() => {
                    fait(msgFound);
                })
                .catch((err) => {
                    return res.status(500).json({ 'error': '[LIKE] Cannot update message like counter' });
                });
            }
        ], (msgFound) => {
            if (msgFound) {
                return res.status(201).json(msgFound);
            } else {
                return res.status(500).json({ 'error': '[LIKE] Cannot update message' });                
            }
        });
    }
}
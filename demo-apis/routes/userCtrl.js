// Les imports
let bcrypt = require('bcrypt');
let models = require('../models');
let jwtUtils = require('../utils/jwt.util');
let asyncLib = require('async');

// Constantes
const EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const PASSWORD_REGEX = /^(?=.*\d).{4,8}$/;
//Les routes
module.exports = {
    register: (req, res) => {
        console.log('Registration');
        // Recupération des params
        let email = req.body.email;
        let username = req.body.username;
        let password = req.body.password;
        let biographie = req.body.biographie;

        if (email == null || username == null || password == null) {
            return res.status(400).json({'error': 'Paramètre manquants !'});
        }
        // TODO Vérifier les informations reçu
        if (username.length >=13 || username.length <=3) {
            return res.status(400).json({'error': 'Invalid username !'});
        }
        if (!EMAIL_REGEX.test(email)) {
            return res.status(400).json({'error': 'Invalid email !'});
        }
        if (PASSWORD_REGEX.test(password)) {
            return res.status(400).json({'error': 'Invalid password (Password must '
                + 'be between 4 and 8 digits long and include at least one numeric digit)'});
        }

        asyncLib.waterfall([
            (fait) => {
                models.User.findOne({
                    attributes: ['email'],
                    where: { email: email }
                }).then((userFound) => {
                    fait(null, userFound);
                }).catch((err) => {
                    return res.status(400).json({ 'error': 'Can\'t verify if user exist or not !'});
                });
            },
            (userFound, fait) => {
                if (!userFound) {
                    bcrypt.hash(password, 5).then((passwordCrypted) => {
                        fait(null, passwordCrypted);
                    })
                    .catch((err) => {
                        return res.status(400).json({'error': 'Impossible de crypter le password !'});
                    })
    
                } else {
                    return res.status(400).json({'error': 'User already exist !'});
                }
            },
            (passwordCrypted, fait) => {
                let newUser = models.User.create({
                    email: email,
                    username: username,
                    password: passwordCrypted,
                    biographie: biographie,
                    isAdmin: 0
                })
                .then((newUser) => fait(newUser))
                .catch((err) => {
                    return res.status(400).json({'error': 'Impossible d\'ajouter l\'user !'});
                });
            }
        ], (newUser) => {
            if (newUser) {
                return res.status(201).json({ 'userId': newUser.id });
            } else {
                return res.status(500).json({'error': 'Impossible d\'ajouter l\'user !'});
            }
        });
    },
    login: (req, res) => {
        console.log('Login');
        // On récupère les params
        let email = req.body.email;
        let password = req.body.password;
        if (email == null || password == null) {
            return res.status(400).json({'error': 'Paramètre manquants !'});
        }
        
        asyncLib.waterfall([
            (fait) => {
                models.User.findOne({
                    where: { email: email }
                })
                .then((userFound) => fait(null, userFound))
                .catch((err) => {
                    return res.status(400).json({'error': 'enable to find account !'});
                });
            },
            (userFound, fait) => {
                if (userFound) {
                    bcrypt.compare(password, userFound.password)
                        .then((dataCrypted) => {
                            fait(null, userFound, dataCrypted);
                        })
                        .catch((err) => {
                            return res.status(400).json({'error': 'enable to compare password !'});
                        }
                    );
                } else {
                    return res.status(400).json({'error': 'Invalid Password or email !'});
                }
            },
            (userFound, dataCrypted, fait) => {
                if (dataCrypted) {
                    fait(userFound);
                } else {
                    return res.status(400).json({'error': 'Invalid Password or Email !'});
                }
            }
        ], (userFound) => {
            if (userFound) {
                return res.status(200).json({
                    'userId': userFound.id,
                    'token': jwtUtils.generationToken(userFound)
                });
            } else {
                return res.status(400).json({'error': 'Authentification impossible ! ' +
                    'Invalid Password or Email !'});
            }
        });       
    },
    getUserProfil: (req, res) => {
        // Getting auth header
        let headerAuth = req.headers['authorization'];
        let userId = jwtUtils.getUserId(headerAuth);
        if (userId < 0) {
            return res.status(400).json({ 'error': 'Token incorrect !' });
        }

        // Find User in Database
        models.User.findOne({
            attributes: ['id', 'email', 'username', 'biographie'],
            where: { id: userId }
        }).then((user) => {
            if (user) 
                res.status(201).json(user);
            else
                return res.status(400).json({ 'error': 'User not found !' });
        }).catch((err) => {
            return res.status(400).json({ 'error': 'Cannot fetch user !' });
        });
    },
    updateUserProfile: (req, res) => {
        let headerAuth = req.headers['authorization'];
        let userId = jwtUtils.getUserId(headerAuth);
        if (userId < 0) {
            return res.status(400).json({ 'error': 'Token incorrect !' });
        }

        // Récupération du paramètre
        let bio = req.body.biographie;
        if (bio == null) {
            return res.status(400).json({ 'error': 'Paramètre manquant' });
        }

        asyncLib.waterfall([
            (fait) => {
                // Find User in Database
                models.User.findOne({
                    attributes: ['id', 'biographie'],
                    where: { id: userId }
                })
                .then((userFound) => fait(null, userFound))
                .catch((err) => {
                    return res.status(500).json({ 'error': '[UPDATE] ' + err.message });
                });
            },
            (userFound, fait) => {
                if (userFound) {
                    userFound.update({
                        biographie: bio
                    })
                    .then(() => fait(userFound))
                    .catch((err) => {
                        return res.status(500).json({ 'error': '[UPDATE] ' + err.message });
                    });
                }
                else return res.status(400).json({ 'error': '[UPDATE] User not found !' });
            }
        ], (userFound) => {
            if (userFound) {
                res.status(201).json(userFound);
            } else {
                return res.status(500).json({ 'error': '[UPDATE] Enable to update this USER !' });
            }
        });
    }
}

let con = require('../config/db')

class Message {

    constructor(row) {
        this.row = row
    }

    get id() {
        return this.row.id
    }

    set id(id) {
        this.row.id = id
    }

    get content() {
        return this.row.content
    }

    get created_at() {
        return this.row.created_at
    }

    static create(content, callback) {
        let message = new Message({
            content: content,
            created_at: new Date()
        })
        con.query('INSERT INTO message SET content = ?, created_at = ?',
            [message.content, message.created_at], (err, result) => {
                let resultat = JSON.parse(JSON.stringify(result));
                message.id = resultat['insertId']
                callback(err, message)
            })
    }

    static loadAll(callback) {
        con.query('SELECT * FROM message', (err, result) => {
                let messages = [];
                result.map(message => messages.push(new Message(message)))
                callback(err, messages)
            }
        )
    }

    static loadOne(id, callback) {
        con.query('SELECT * FROM message WHERE id = ?', [id], (err, result) => {
                callback(err, new Message(result[0]))
            }
        )
    }
}
module.exports = Message
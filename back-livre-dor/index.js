let express = require('express')
let port = process.env.PORT || 8082;

let app = express()
let MessageController = require('./controller/messageController')

let cors = require('cors')
let whitelist = ['http://localhost:4200']
let corsOptions = {
  origin: function (origin, callback) {
    if (whitelist.indexOf(origin) !== -1) {
      callback(null, true)
    } else {
      callback(new Error('Not allowed by CORS'))
    }
  }
}

// Les Middelwares
app.use(cors(corsOptions))

// Les Controllers
app.use('/messages', MessageController)

app.listen(port, () => {
    console.log('Express server listening on port ' + port)
    const all_routes = require('express-list-endpoints')
    console.log(all_routes(app))
    console.log('====== // Waiting Logs // ======')
})

let express = require('express');
let router = express.Router();
let bodyParser = require('body-parser');
let Message = require('../models/message')


// Les middlewares
router.use(bodyParser.urlencoded({ extended: false }))
router.use(bodyParser.json());

// Les API
/**
 * API de récupération des messages
 */
router.get('/', (req, res) => {
    Message.loadAll((err, result) => {
        if (err) {
            res.status(400).json(err)
        } else {
            res.json(result)
        }
    })
})

/**
 * API de création de message
 */
router.post('/', (req, res) => {
    Message.create(req.body.content, (err, result) => {
        if (err) {
            res.status(500).json(err)
        } else {
            console.log('Le commentaire avec l\'ID ' + result.id + ' a bien été enregistré' )
            res.json(result)
        }
    })
})

/**
 * API de réccupération d'un message
 */
router.get('/:id', (req, res) => {
    Message.loadOne(req.params.id, (err, result) => {
        if (err) {
            res.status(500).json(err)
        } else {
            res.json(result)
        }
    })
})

module.exports = router;